import os
import requests
from openapi_spec_validator import validate_spec_url


##! --- Test for blueprint_x -- !##
def test_blueprint_x_test(api_v1_host):
    endpoint = os.path.join(api_v1_host, "blueprint_x", "test")
    response = requests.get(endpoint)

    assert response.status_code == 200

    json = response.json()

    assert "message" in json
    assert json["message"] == "I'm the test endpoint from blueprint_x."


def test_blueprint_x_plus(api_v1_host):
    endpoint = os.path.join(api_v1_host, "blueprint_x", "plus")
    payload = {"number": 100}
    response = requests.post(endpoint, json=payload)

    assert response.status_code == 200

    json = response.json()

    assert "message" in json
    assert json["message"] == f"Your result is: \"{ payload['number'] + 5}\""


##? --- END --- ?##


##! --- Test for blueprint_y -- !##
def test_blueprint_y_test(api_v1_host):
    endpoint = os.path.join(api_v1_host, "blueprint_y", "test")
    response = requests.get(endpoint)

    assert response.status_code == 200

    json = response.json()

    assert "message" in json
    assert json["message"] == "I'm the test endpoint from blueprint_y."


def test_blueprint_y_minus(api_v1_host):
    endpoint = os.path.join(api_v1_host, "blueprint_y", "minus")
    payload = {"number": 1000}
    response = requests.post(endpoint, json=payload)

    assert response.status_code == 200

    json = response.json()

    assert "message" in json
    assert json["message"] == f"Your result is: \"{payload['number'] - 1000}\""


##? --- END --- ?##


##! --- Test for swagger -- !##
def test_swagger_specification(host):
    endpoint = os.path.join(host, "api", "swagger.json")
    validate_spec_url(endpoint)


##? --- END --- ?##
