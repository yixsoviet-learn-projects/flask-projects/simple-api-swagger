##! --- For Production --- !##

from src.app import app

if __name__ == "__main__":
    ## For DEV
    app.run(host="0.0.0.0", debug=True)
