from flask import Blueprint, jsonify, request

blueprint_y = Blueprint(name="blueprint_y", import_name=__name__)

local_number = 1000


@blueprint_y.route("/test", methods=["GET"])
def test():
    response = {"message": "I'm the test endpoint from blueprint_y."}

    return jsonify(response)


@blueprint_y.route("minus", methods=["POST"])
def plus_x():
    data = request.get_json()
    in_val = data["number"]
    result = in_val - local_number
    response = {"message": f'Your result is: "{result}"'}

    return jsonify(response)
