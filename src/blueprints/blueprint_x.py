from flask import Blueprint, jsonify, request

blueprint_x = Blueprint(name="blueprint_x", import_name=__name__)

local_number = 5


@blueprint_x.route("/test", methods=["GET"])
def test():
    """
    ---
    get:
      description: test endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: OutputSchema
      tags:
          - testing
    """
    response = {"message": "I'm the test endpoint from blueprint_x."}

    return jsonify(response)


@blueprint_x.route("/plus", methods=["POST"])
def plus_x():
    """
    ---
    post:
      description: increments the input by x
      requestBody:
        required: true
        content:
            application/json:
                schema: InputSchema
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: OutputSchema
      tags:
          - calculation
    """
    data = request.get_json()
    in_val = data["number"]
    result = in_val + local_number
    response = {"message": f'Your result is: "{result}"'}

    return jsonify(response)
